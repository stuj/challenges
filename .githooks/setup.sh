#!/usr/bin/env bash
SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
deactivate 2>/dev/null || true
pip3 install -r "${SCRIPT_DIR}/requirements.txt"
pre-commit install
pre-commit run -a
