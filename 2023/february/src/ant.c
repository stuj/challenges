#include "ant.h"

#include "cell.h"

Ant getAnt(int x, int y, int heading) {
  return (Ant){.x = x, .y = y, .heading = heading};
}

Cell* getAntCell(Ant ant) { return getCellAt(ant.x, ant.y); }

char getAntChar(Ant ant) {
  switch (ant.heading) {
    case 0:
      return '^';
    case 90:
      return '>';
    case 180:
      return 'v';
    case 270:
      return '<';
    default:
      return 'X';
  }
}

void rotateAnt(Ant* ant, Cell* antCell) {
  if (antCell->isBlack) {
    ant->heading += 90;
  } else {
    ant->heading += 270;
  }

  ant->heading = ant->heading % 360;
}

void moveAnt(Ant* ant, Cell* antCell) {
  switch (ant->heading) {
    case 0:
      ant->y -= 1;
      break;
    case 90:
      ant->x += 1;
      break;
    case 180:
      ant->y += 1;
      break;
    case 270:
      ant->x -= 1;
      break;
  }

  antCell->isBlack ^= true;
}
