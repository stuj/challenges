#include "ant.h"
#include "cell.h"
#include "draw.h"
#include "interrupt.h"
#include "sleep.h"

#define true 1
#define false 0

#define INITIAL_GRID_WIDTH 7
#define INITIAL_GRID_HEIGHT 8
#define INITIAL_ANT_X_POSITION 3
#define INITIAL_ANT_Y_POSITION 3
#define INITIAL_ANT_HEADING 90  // degrees - i.e. east, i.e. facing right

int main() {
  initializeCells(INITIAL_GRID_WIDTH, INITIAL_GRID_HEIGHT);
  Ant ant = getAnt(INITIAL_ANT_X_POSITION, INITIAL_ANT_Y_POSITION,
                   INITIAL_ANT_HEADING);
  drawInit();
  subscribeInterrupts();

  while (!interrupted) {
    Cell* antCell = getAntCell(ant);
    draw(ant, antCell);
    fullSleep();

    rotateAnt(&ant, antCell);
    draw(ant, antCell);
    fullSleep();

    moveAnt(&ant, antCell);
    fullRedrawRequired = checkCells(ant);
    draw(ant, antCell);
    miniSleep();
  }

  drawEnd();
  releaseCells();
  return 0;
}
