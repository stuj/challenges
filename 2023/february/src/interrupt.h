#ifndef INTERRUPT_H
#define INTERRUPT_H

#include <signal.h>
#define true 1
#define false 0

volatile sig_atomic_t interrupted;

void subscribeInterrupts();
void handleInterrupts(int signal);
#endif
