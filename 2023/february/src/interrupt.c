#include "interrupt.h"

#include <signal.h>

void subscribeInterrupts() {
  signal(SIGINT, handleInterrupts);
  interrupted = false;
}

void handleInterrupts(int signal) {
  if (signal == SIGINT) {
    interrupted = true;
  }
}
