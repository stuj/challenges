#ifndef CELL_H
#define CELL_H
#include "types.h"

Cell* top_left;
Cell* bottom_right;

int checkCells(Ant ant);
Cell* newCell(int x, int y);
void initializeCells(int width, int height);
Cell* getCellAt(int x, int y);
void expandCellGrid();
void releaseCells();
#endif
