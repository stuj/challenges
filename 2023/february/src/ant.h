#ifndef ANT_H
#define ANT_H
#include "types.h"
#define true 1
#define false 0

Ant getAnt(int x, int y, int heading);
Cell* getAntCell(Ant ant);
char getAntChar(Ant ant);
void rotateAnt(Ant* ant, Cell* antCell);
void moveAnt(Ant* ant, Cell* antCell);

#endif
