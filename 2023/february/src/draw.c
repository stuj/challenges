#include <ncurses.h>

#include "ant.h"
#include "cell.h"

int fullRedrawRequired = 1;

void drawCell(Cell* cell, Ant ant);

void drawInit() {
  initscr();
  cbreak();
  curs_set(0);
  start_color();
  init_pair(1, COLOR_WHITE, COLOR_MAGENTA);
  init_pair(2, COLOR_BLACK, COLOR_WHITE);
}

void draw(Ant ant, Cell* redraw) {
  int row, col;
  getmaxyx(stdscr, row, col);

  int width = bottom_right->x - top_left->x;
  int height = bottom_right->y - top_left->y;

  if (width + 1 > row || height + 1 > col) {
    fullRedrawRequired = true;
    return;
  }

  if (!fullRedrawRequired) {
    drawCell(redraw, ant);
    if (redraw->x != ant.x || redraw->y != ant.y) {
      Cell* cell = getAntCell(ant);
      drawCell(cell, ant);
    }
  } else {
    Cell* row_start = top_left;
    while (row_start != NULL) {
      Cell* cell = row_start;
      while (cell != NULL) {
        drawCell(cell, ant);
        cell = cell->right;
      }
      row_start = row_start->below;
    }
  }
  refresh();
}

void drawEnd() {
  curs_set(1);
  endwin();
}

void drawCell(Cell* cell, Ant ant) {
  int color_pair_index = 2;
  if (cell->isBlack) {
    color_pair_index = 1;
  }

  int x = cell->x - top_left->x;
  int y = cell->y - top_left->y;
  char p = ' ';

  if (cell->x == ant.x && cell->y == ant.y) {
    p = getAntChar(ant);
  }

  move(y, x);
  attron(COLOR_PAIR(color_pair_index));
  addch(p);
  attroff(COLOR_PAIR(color_pair_index));
}
