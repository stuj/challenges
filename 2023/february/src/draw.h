#ifndef DRAW_H
#define DRAW_H
#include "ant.h"
#include "cell.h"

int fullRedrawRequired;

void drawInit();
void draw(Ant ant, Cell* redraw);
void drawEnd();
#endif
