#include <time.h>

const long sleepDuration = 1000000 * 300;  // 0.3 seconds in nanoseconds
struct timespec sleepTime = (struct timespec){.tv_nsec = sleepDuration};
struct timespec miniSleepTime = (struct timespec){.tv_nsec = sleepDuration / 2};

void fullSleep() { nanosleep(&sleepTime, NULL); }

void miniSleep() { nanosleep(&miniSleepTime, NULL); }
