#include "cell.h"

#include <stdlib.h>

#include "ant.h"

#define true 1
#define false 0

Cell* top_left;
Cell* bottom_right;

int checkCells(Ant ant) {
  if (ant.x < top_left->x || ant.y < top_left->y || ant.x > bottom_right->x ||
      ant.y > bottom_right->y) {
    expandCellGrid();
    return true;
  }

  return false;
}

Cell* newCell(int x, int y) {
  Cell* new = malloc(sizeof(Cell));
  new->x = x;
  new->y = y;
  new->isBlack = false;
  new->above = NULL;
  new->below = NULL;
  new->left = NULL;
  new->right = NULL;
  return new;
}

void initializeCells(int width, int height) {
  Cell* cells[width * height];
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      cells[x + (y * width)] = newCell(x, y);
    }
  }

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      Cell* cell = cells[x + (y * width)];

      if ((x - 1) >= 0) {
        cell->left = cells[x - 1 + (y * width)];
      }

      if ((x + 1) < width) {
        cell->right = cells[x + 1 + (y * width)];
      }

      if ((y - 1) >= 0) {
        cell->above = cells[x + ((y - 1) * width)];
      }

      if ((y + 1) < height) {
        cell->below = cells[x + ((y + 1) * width)];
      }
    }
  }

  top_left = cells[0];
  bottom_right = cells[(width * height) - 1];
}

Cell* getCellAt(int x, int y) {
  Cell* current_cell = top_left;
  while (current_cell->x < x) {
    current_cell = current_cell->right;
    if (current_cell == NULL) {
      return NULL;
    }
  }

  while (current_cell->y < y) {
    current_cell = current_cell->below;
    if (current_cell == NULL) {
      return NULL;
    }
  }

  return current_cell;
}

void expandCellGrid() {
  Cell* previous = NULL;

  // Expand to left
  for (int y = top_left->y; y <= bottom_right->y; y++) {
    Cell* c = newCell(top_left->x - 1, y);

    if (previous == NULL) {
      c->right = top_left;
      c->right->left = c;
    } else {
      c->above = previous;
      c->above->below = c;
      c->right = c->above->right->below;
      c->right->left = c;
    }

    previous = c;
  }

  top_left = top_left->left;
  previous = NULL;

  // Expand to top
  for (int x = top_left->x; x <= bottom_right->x; x++) {
    Cell* c = newCell(x, top_left->y - 1);

    if (previous == NULL) {
      c->below = top_left;
      c->below->above = c;
    } else {
      c->left = previous;
      c->left->right = c;
      c->below = c->left->below->right;
      c->below->above = c;
    }

    previous = c;
  }

  top_left = top_left->above;
  previous = NULL;

  // Expand to right
  for (int y = bottom_right->y; y >= top_left->y; y--) {
    Cell* c = newCell(bottom_right->x + 1, y);

    if (previous == NULL) {
      c->left = bottom_right;
      c->left->right = c;
    } else {
      c->below = previous;
      c->below->above = c;
      c->left = c->below->left->above;
      c->left->right = c;
    }

    previous = c;
  }

  bottom_right = bottom_right->right;
  previous = NULL;

  // Expand to bottom
  for (int x = bottom_right->x; x >= top_left->x; x--) {
    Cell* c = newCell(x, bottom_right->y + 1);

    if (previous == NULL) {
      c->above = bottom_right;
      c->above->below = c;
    } else {
      c->right = previous;
      c->right->left = c;
      c->above = c->right->above->left;
      c->above->below = c;
    }

    previous = c;
  }

  bottom_right = bottom_right->below;
}

void releaseCells() {
  Cell* row_start = top_left;
  while (row_start != NULL) {
    Cell* cell = row_start;
    Cell* next_row_start = cell->below;
    while (cell != NULL) {
      Cell* next_cell = cell->right;
      free(cell);
      cell = next_cell;
    }
    row_start = next_row_start;
  }

  top_left = NULL;
  bottom_right = NULL;
}
