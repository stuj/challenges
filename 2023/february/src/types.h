#ifndef TYPES_H
#define TYPES_H

typedef struct {
  int x;
  int y;
  int heading;
} Ant;

typedef struct Cell Cell;

struct Cell {
  int x;
  int y;
  Cell* above;
  Cell* below;
  Cell* left;
  Cell* right;
  int isBlack;
};

#endif
