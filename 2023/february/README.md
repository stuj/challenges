# February 2023 Challenge

## Language and UI choice

* ANSI C (not C++)

* ncurses

## Usage

* `cd` into the `challenges\2023\february` directory

* Run `make build` to build the code

* Run `make run` to run the built artifact

## Prerequisites

* You will need `cmake` installed, you can install using homebrew `brew install --cask cmake`

## Compatibility

* Tested on Intel Macbook

* May work on other Linux/Mac distributions and architectures, but not tested
