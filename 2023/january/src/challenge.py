import json
from typing import Callable, Tuple


def challenge(input: str) -> str:
    clipboard = ""

    def cut(input: str, pos: int) -> str:
        nonlocal clipboard
        clipboard = input[:pos]
        return input[pos + 8:]

    def copy(input: str, pos: int) -> str:
        nonlocal clipboard
        clipboard = input[:pos]
        return clipboard + input[pos + 8:]

    def paste(input: str, pos: int) -> str:
        nonlocal clipboard
        return input[:pos] + clipboard + input[pos + 8:]

    def get_next_command(input: str) -> Tuple[int, Callable[[str, int], str]]:
        positions = [(input.find(command[0]), *command) for command in commands]
        valid_positions = [position for position in positions if position[0] != -1]
        if len(valid_positions) == 0:
            return None
        next_position = min(valid_positions, key=lambda p: p[0])
        return (next_position[0], next_position[2])

    commands = [("[CTRL+X]", cut), ("[CTRL+C]", copy), ("[CTRL+V]", paste)]

    while True:
        next_command = get_next_command(input)
        if not next_command:
            return input
        input = next_command[1](input, next_command[0])


def main():
    import sys

    with open(sys.argv[1], "r") as fp:
        data = json.load(fp)

    print("\n" + ("*" * 80))
    for item in data:
        print(item)
        print(challenge(item))
        print("*" * 80)
    print()


if __name__ == "__main__":
    main()
