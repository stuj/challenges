import unittest
from io import StringIO

from challenge import challenge


class ClipboardTests(unittest.TestCase):
    def test_empty_string(self):
        self.assertEqual(challenge(""), "")

    def test_remove_ctrl_c(self):
        self.assertNotIn("[CTRL+C]", challenge("[CTRL+C]"))

    def test_remove_ctrl_v(self):
        self.assertNotIn("[CTRL+V]", challenge("[CTRL+V]"))

    def test_remove_ctrl_x(self):
        self.assertNotIn("[CTRL+X]", challenge("[CTRL+X]"))

    def test_cut_paste_valid(self):
        self.assertEqual(
            "it should be pasted after here this should be cut but before here",
            challenge(
                "this should be cut [CTRL+X]it should be pasted after here [CTRL+V]but before here"
            ),
        )

    def test_cut_paste_wrong_order(self):
        self.assertEqual(
            "and this should still be here",
            challenge(
                "nothing should be pasted here [CTRL+V]but this should be cut [CTRL+X]and this should still be here"
            ),
        )

    def test_cut_only(self):
        self.assertEqual(
            "and not pasted", challenge("this should be cut [CTRL+X]and not pasted")
        )

    def test_copy_paste_valid(self):
        self.assertEqual(
            "this should be copied it should be pasted after here this should be copied but before here",
            challenge(
                "this should be copied [CTRL+C]it should be pasted after here [CTRL+V]but before here"
            ),
        )

    def test_copy_paste_wrong_order(self):
        self.assertEqual(
            "nothing should be pasted here and this should still be here and so should this",
            challenge(
                "nothing should be pasted here [CTRL+V]and this should still be here [CTRL+C]and so should this"
            ),
        )

    def test_copy_only(self):
        self.assertEqual(
            "this should be be copied and not pasted",
            challenge("this should be be copied [CTRL+C]and not pasted"),
        )

    def test_sample_cases(self):
        test_data = [
            (
                "the big red[CTRL+C] fox jumps over [CTRL+V] lazy dog.",
                "the big red fox jumps over the big red lazy dog.",
            ),
            (
                "[CTRL+V]the tall oak tree towers over the lush green meadow.",
                "the tall oak tree towers over the lush green meadow.",
            ),
            (
                "the sun shines down[CTRL+C] on [CTRL+V][CTRL+C] the busy [CTRL+V].",
                "the sun shines down on the sun shines down the busy the sun shines down on the sun shines down.",
            ),
            (
                "[CTRL+V]the tall oak tree towers over the lush green meadow.",
                "the tall oak tree towers over the lush green meadow.",
            ),
            (
                "a majestic lion[CTRL+C] searches for [CTRL+V] in the tall grass.",
                "a majestic lion searches for a majestic lion in the tall grass.",
            ),
            (
                "the shimmering star[CTRL+X]Twinkling in the dark, [CTRL+V] shines bright.",
                "Twinkling in the dark, the shimmering star shines bright.",
            ),
            (
                "[CTRL+X]a fluffy white cloud drifts [CTRL+V][CTRL+C] across the sky, [CTRL+V]",
                "a fluffy white cloud drifts  across the sky, a fluffy white cloud drifts ",
            ),
        ]
        for input, expected in test_data:
            self.assertEqual(expected, challenge(input))

    def test_main(self):
        import sys

        from challenge import main
        backup_args = sys.argv
        backup_stdout = sys.stdout
        stdout_proxy = StringIO()
        try:
            sys.stdout = stdout_proxy
            sys.argv = [None, "../data/input.json"]
            main()
        finally:
            sys.argv = backup_args
            sys.stdout = backup_stdout
            stdout_proxy.seek(0)
            data = stdout_proxy.read().split("\n")
            stdout_proxy.close()
            self.assertEqual(len(data), 25)


if __name__ == "__main__":
    unittest.main()
