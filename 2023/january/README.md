# January 2023 Challenge

## Usage

* `cd` into the `challenges\2023\january` directory

* Run `make run` to run the challenge displaying the input and transformed output from the data file `data/input.json`

* Run `make test` to run python unit tests
