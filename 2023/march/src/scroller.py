import curses
from os import get_terminal_size
from time import sleep
from typing import List


class Scroller:
    _colors = {
        "WHITE": 0,
        "BLACK": 1,
        "BLUE": 2,
        "CYAN": 3,
        "GREEN": 4,
        "MAGENTA": 5,
        "RED": 6,
        "YELLOW": 7
    }

    def _get_color(color_name: str):
        color_index = Scroller._colors.get(color_name, 0)
        return curses.color_pair(color_index)

    def __init__(self, scroll_text, scroll_width, scroll_cps):
        self._stdscr = curses.initscr()
        curses.curs_set(0)
        curses.cbreak()
        curses.start_color()
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(7, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        self._stdscr.timeout(0)
        self._scroll_text = scroll_text
        self._scroll_width = scroll_width
        self._scroll_cps = scroll_cps
        self._bold_attrs = [curses.A_NORMAL] * len(scroll_text)
        self._underline_attrs = [curses.A_NORMAL] * len(scroll_text)
        self._color_attrs = [curses.color_pair(0)] * len(scroll_text)
        self._pad = None

    def __del__(self):
        curses.curs_set(1)
        curses.nocbreak()
        curses.endwin()

    def set_bold(self, bold_flags: List[bool]):
        self._bold_attrs = [curses.A_BOLD if flag else curses.A_NORMAL for flag in bold_flags]

    def set_underline(self, underline_flags: List[bool]):
        self._underline_attrs = [curses.A_UNDERLINE if flag else curses.A_NORMAL for flag in underline_flags]

    def set_colors(self, color_flags: List[str]):
        self._color_attrs = [Scroller._get_color(flag) for flag in color_flags]

    def render(self):
        default_color = Scroller._get_color(None)
        combi_attrs = [self._bold_attrs[ix] | self._underline_attrs[ix] | self._color_attrs[ix]
                       for ix in range(0, len(self._scroll_text))]
        padded_scroll_text = self._scroll_text + " " + (3*". ")
        combi_attrs = combi_attrs + ([default_color] * 7)
        while len(padded_scroll_text) < self._scroll_width:
            padded_scroll_text += ". "
            combi_attrs = combi_attrs + ([default_color] * 2)
        padded_scroll_text = padded_scroll_text * 2
        combi_attrs = combi_attrs * 2
        self._pad = curses.newpad(2, len(padded_scroll_text) + 1)
        self._pad.addstr(0, 0, padded_scroll_text)
        for ix in range(0, len(combi_attrs)):
            self._pad.chgat(0, ix, combi_attrs[ix])

    def run(self):
        if not self._pad:
            return
        _, width = self._pad.getmaxyx()
        while True:
            for i in range(0, int(width / 2)):
                if self._scroll_width > (get_terminal_size().columns - 1):
                    self._scroll_width = (get_terminal_size().columns - 1)
                self._pad.refresh(0, i, 0, 0, 1, self._scroll_width)
                sleep(1 / self._scroll_cps)
                ch = self._stdscr.getch()
                if ch not in [-1, 410]:
                    return
