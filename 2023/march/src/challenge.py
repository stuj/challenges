from os import environ, get_terminal_size, getcwd

from scroller import Scroller
from tokenizer import TokenSet


def scroll(scroll_text: str, scroll_width: str = None, scroll_cps: int = 5):

    try:
        token_set = TokenSet(scroll_text)
        scroller = Scroller(token_set.string_data, scroll_width, scroll_cps)
        scroller.set_bold(token_set.bold_flags)
        scroller.set_underline(token_set.underline_flags)
        scroller.set_colors(token_set.color_flags)
        scroller.render()
        scroller.run()
    finally:
        del scroller


def get_int_value(env_name, default):
    env_val = environ.get(env_name, None)
    if env_val:
        try:
            val = int(env_val)
            return val
        except Exception:
            return default
    return default


def get_text_file(env_name, default):
    env_val = environ.get(env_name, None)
    if env_val:
        try:
            with open(f"{getcwd()}/../{env_val}", "r") as f:
                val = f.readline().strip("\n")
                return val
        except Exception:
            return default
    return default


default_text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890 "
default_text = ("[C:" + (f"]{default_text}[/C][C:".join(["WHITE", "BLACK", "RED",
                "CYAN", "GREEN", "MAGENTA", "YELLOW", "BLUE"])) + f"{default_text}[/C]")
default_text = f"{default_text}[B]{default_text}[U]{default_text}[/B]{default_text}[/U]"
default_width = get_terminal_size().columns - 1
default_speed = 8

speed = get_int_value("SCROLL_SPEED", default_speed)
width = get_int_value("SCROLL_WIDTH", default_width)
text = get_text_file("SCROLL_FILE", default_text)

scroll(text, width, speed)
