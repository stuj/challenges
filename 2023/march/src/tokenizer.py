from enum import Enum
from typing import List, Tuple


class _TokenType(Enum):
    STRING = 0
    UNKNOWN_TAG = 1
    COLOR_ON = 2
    COLOR_OFF = 3
    BOLD_ON = 4
    BOLD_OFF = 5
    UNDERLINE_ON = 6
    UNDERLINE_OFF = 7


class _Token:
    def __init__(self, data: str, token_type: _TokenType, position: int) -> None:
        self.type = token_type
        self.data = data
        self.position = position
        if self.type == _TokenType.UNKNOWN_TAG and len(self.data) > 0:
            if self.data == "B":
                self.type = _TokenType.BOLD_ON
                self.data = None
            elif self.data == "/B":
                self.type = _TokenType.BOLD_OFF
                self.data = None
            elif self.data == "U":
                self.type = _TokenType.UNDERLINE_ON
                self.data = None
            elif self.data == "/U":
                self.type = _TokenType.UNDERLINE_OFF
                self.data = None
            elif self.data == "/C":
                self.type = _TokenType.COLOR_OFF
                self.data = None
            elif len(self.data) > 2 and self.data[0:2] == "C:":
                self.type = _TokenType.COLOR_ON
                self.data = self.data[2:]

    def __repr__(self) -> str:
        return str(self.type) + ((":" + self.data) if self.data else "") + f"@{self.position}"


class TokenSet:
    def _parse_next_token(data: str, token_position: int) -> Tuple[_Token, str]:
        token_type = _TokenType.STRING
        token_data = ""  # nosec
        escape_flag = False
        for ix in range(0, len(data)):
            if ix == 0 and data[ix] == "[":
                token_type = _TokenType.UNKNOWN_TAG
            elif data[ix] == "\\" and not escape_flag:
                escape_flag = True
            elif data[ix] not in ["[", "]"] and escape_flag:
                escape_flag = False
                token_data += ("\\" + data[ix])
            elif data[ix] in ["[", "]"] and escape_flag:
                token_data += data[ix]
                escape_flag = False
            elif data[ix] == "[" and token_type == _TokenType.STRING:
                return (_Token(token_data, token_type, token_position), data[ix:])
            elif data[ix] == "]" and token_type == _TokenType.UNKNOWN_TAG:
                return (_Token(token_data, token_type, token_position), data[ix+1:])
            else:
                token_data += data[ix]
        return (_Token(token_data, token_type, token_position), None)

    def __init__(self, data: str) -> None:
        token_position = 0
        result = []
        remaining_data = data
        while remaining_data:
            token, remaining_data = TokenSet._parse_next_token(remaining_data, token_position)
            result.append(token)
            if token.type == _TokenType.STRING:
                token_position += len(token.data)
        self._all_tokens = result
        self.string_data = "".join([token.data for token in self._all_tokens if token.type == _TokenType.STRING])
        self.bold_flags = self._get_boolean_flags(_TokenType.BOLD_ON, _TokenType.BOLD_OFF)
        self.underline_flags = self._get_boolean_flags(_TokenType.UNDERLINE_ON, _TokenType.UNDERLINE_OFF)
        self.color_flags = self._get_stack_flags(_TokenType.COLOR_ON, _TokenType.COLOR_OFF)

    def _get_boolean_flags(self, token_type_on: _TokenType, token_type_off: _TokenType) -> List[bool]:
        total_length = len(self.string_data)
        result = [False] * total_length
        matching_tokens = [token for token in self._all_tokens if token.type in [token_type_on, token_type_off]]
        if not matching_tokens:
            return result
        match_counter = 0
        for matching_token in matching_tokens:
            if matching_token.type == token_type_on:
                match_counter += 1
            else:
                match_counter = max(0, match_counter - 1)
            for ix in range(matching_token.position, total_length):
                result[ix] = match_counter != 0
        return result

    def _get_stack_flags(self, token_type_on: _TokenType, token_type_off: _TokenType) -> List[str]:
        total_length = len(self.string_data)
        result = [None] * total_length
        matching_tokens = [token for token in self._all_tokens if token.type in [token_type_on, token_type_off]]
        if not matching_tokens:
            return result
        stack = []
        for matching_token in matching_tokens:
            if matching_token.type == token_type_on:
                stack.append(matching_token.data)
            elif len(stack) > 0:
                stack = stack[:-1]
            for ix in range(matching_token.position, total_length):
                result[ix] = None if len(stack) == 0 else stack[-1]
        return result
