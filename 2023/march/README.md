# March 2023 Challenge

## Usage

* `cd` into the `challenges/2023/march` directory

* Run `make run` to run the challenge using the default settings

    * Scroll width - the width of the terminal
    * Scroll speed - 8 cps (characters per second)
    * Scroll text - a demo string showcasing different colours, bold, underline, and combinations

---

### Overriding Parameters

The Scroll Width, Scroll Speed, and Scroll Text can be overridden using the following Environment Variables:

* `SCROLL_WIDTH`
* `SCROLL_SPEED`
* `SCROLL_FILE`
    * This is the path of a text file relative to the `challenges/2023/march` directory
    * Only the first line of this file will be read - subsequent lines will be ignored

For example:
```
$ SCROLL_WIDTH=40 SCROLL_SPEED=20 SCROLL_FILE="data/sample.txt" make run
```
will scroll the text contained in the first line of the `challenges/2023/march/data/sample.txt` file, at a maximum width of 40 characters, at a speed of 20 characters per second.

---

### Tags

#### <ins>Types</ins>

 The following tags are supported to control the appearance of the scrolling text:

 * `[C:COLORNAME]...[/C]`
    * Changes the text colour to one of the following supported colours (all on a black background unless otherwise stated):
        * `WHITE`
        * `BLACK` (on white background)
        * `CYAN`
        * `MAGENTA`
        * `YELLOW`
        * `RED`
        * `BLUE`
        * `GREEN`
 * `[B]...[/B]`
    * Displays the text in __bold__
 * `[U]...[/U]`
    * <ins>Underlines</ins> the text

 #### <ins>Nesting</ins>

Tags of different types can be nested permissively, for example the following construct is allowed - the nesting is only taken into consideration for each individual tag type:
```
[B]This is bold [U] and underlined [/B]but not bold any more[/U]
```
Invalid tags (including colour tags with invalid colour names) will be ignored.

To render `[` or `]`, you can escape the tag characters with a backslash, i.e. `\[` or `\]`

---

### Behaviours

* The requested scroll width will be ignored if it's larger than the terminal width - the terminal width will be used instead.

* If the terminal is resized wider, the scroll width will remain as it was prior to the resize.

* If the terminal is resized narrower than the scroll width, the scroll width will be permanently reduced to the new terminal width.
